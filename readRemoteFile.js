var request = require('request'),
	fs = require('fs'),
	Rx = require('rx'),
	titleCase = require('./titleCase.js');

var inputWriteStream = fs.createWriteStream('titlecase_input.txt');
var outputWriteStream = fs.createWriteStream('titlecase_output.txt');

request('https://www.dropbox.com/s/kf1j2x8i5py7213/titlecase_input.txt?dl=1').pipe(inputWriteStream);

inputWriteStream.on('finish', function () {

	var readStream = fs.createReadStream(inputWriteStream.path);

	var source = Rx.Observable.fromEvent(readStream, "data")
		.map(titleCase);	

	var subscription = source.subscribe(
	    function (x) {
	    	outputWriteStream.write(x);
	    },
	    function (err) {
	        console.log('Error: ' + err);
	    },
	    function () {
	        console.log('Completed');
	    }
    );

});

